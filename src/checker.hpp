#pragma once

namespace sop_bds {

int64_t checker(InstanceSOP& inst, std::vector<NodeId> sol) {
    // check if size respected
    if ( static_cast<int>(sol.size()) != inst.get_nb_vertices() ) {
        return -1;
    }
    // check if start and end not respected
    if ( sol[0] != 0 && sol[sol.size()-1] != sol.size()-1 ) {
        return -2;
    }
    // check precedence constraints
    std::vector<int> positions(sol.size(), 0);
    for ( uint32_t i = 0 ; i < sol.size() ; i++ ) {
        positions[sol[i]] = i;
    }
    for ( Precedence p : inst.getPrecedences() ) {
        if ( positions[p.first] > positions[p.second] ) return -4;
    }
    // check precedence constraints (bis)
    for ( uint16_t i = 1 ; i < sol.size() ; i++ ) {
        for ( int j = 0 ; j < i ; j++ ) {
            if ( inst.is_precedence(sol[i], sol[j]) ) {
                return -3;
            }
        }
    }
    // if solution admissible, compute cost
    int64_t cost = 0;
    for ( uint32_t i = 1 ; i < sol.size() ; i++ ) {
        cost += inst.get_weight(sol[i-1], sol[i]);
    }
    return cost;
}

}  // namespace sop_bds
