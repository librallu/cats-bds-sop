#pragma once
#include <string>
#include <iostream>
#include <cassert>
#include <cmath>
#include <memory>
#include <random>

#include "../../cats-framework/include/Node.hpp"
#include "../../cats-framework/include/numeric.hpp"

#include "InstanceSOP.hpp"
#include "StoreSOP.hpp"
#include "checker.hpp"

using namespace cats;

namespace sop_bds {

class NodeForw : public PrefixEquivalenceNode<PrefixEquivalenceSOP> {
 private:
    InstanceSOP& inst_;
    std::vector<NodeId> prefix_;
    Weight cost_prefix_;
    SubsetInt added_subset_;
    std::string& output_filename_;

 public:
    explicit NodeForw(InstanceSOP& inst, std::string& output_filename): Node(),
        inst_(inst), cost_prefix_(0), added_subset_(), output_filename_(output_filename) {
            prefix_.push_back(inst_.getStartVertex());
            added_subset_.add(inst_.getStartVertex());
        }

    explicit NodeForw(const NodeForw& s): Node(s),
        inst_(s.inst_), prefix_(s.prefix_), cost_prefix_(s.cost_prefix_),
        added_subset_(s.added_subset_), output_filename_(s.output_filename_) {}

    inline NodePtr copy() const override { return NodePtr(new NodeForw(*this)); }

    inline double evalPrefix() const override {
        return cost_prefix_;
    }

    inline double guide() override {
        return cost_prefix_;
    }

    inline std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> res;
        NodeId last_city_ = lastCity();
        // for each vertex, check if we can add it
        for ( NodeId neigh : inst_.get_neighbours_by_rank(last_city_) ) {
            // check that it is not already added and preds satisfied
            bool to_add = !added_subset_.contains(neigh) && added_subset_.include(inst_.get_pred_set(neigh));
            if ( to_add ) {  // generate children and add it to the result
                NodeForw* child = new NodeForw(*this);
                child->add_city(neigh);
                res.push_back(NodePtr(child));
            }
        }
        return res;
    }

    inline bool isGoal() const override {
        return inst_.get_nb_vertices() == static_cast<int>(prefix_.size());
    }

    /**
     * \brief called when a new best solution is found. Checks that the solution is feasible. If not, explain why
     */
    void handleNewBest() override {
        double check_val = checker(inst_, prefix_);
        assert(check_val == this->evaluate());
        // write solution file
        std::ofstream f;
        f.open(output_filename_);
        for ( NodeId p : prefix_ ) {
            f << p << " ";
        }
        f << "\n";
        f.close();
    }

    inline PrefixEquivalenceSOP getPrefixEquivalence() const override {
        return PrefixEquivalenceSOP(added_subset_, prefix_[prefix_.size()-1]);
    }

    std::string getName() override {
        return "forward";
    }

    inline NodeId lastCity() { return prefix_[prefix_.size()-1]; }

 private:
    /**
     * \brief adds a given city to the current node
     */
    inline void add_city(NodeId j) {
        NodeId i = lastCity();
        added_subset_.add(j);
        prefix_.push_back(j);
        // update bounds
        cost_prefix_ += inst_.get_weight(i, j);
    }
};

}  // namespace sop_bds
