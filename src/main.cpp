#include <iostream>
#include <set>
#include <omp.h>
#include <signal.h>

#include "../../cats-framework/include/util/includeAlgorithms.hpp"
#include "../../cats-framework/include/SearchManager.hpp"

#include "InstanceSOP.hpp"
#include "NodeForw.hpp"
#include "NodeBack.hpp"
#include "NodeFB.hpp"
#include "StoreSOP.hpp"

using namespace cats;
using namespace sop_bds;

/**
 * global objects used by the end search handler
 */
SearchManager search_manager = SearchManager();

// GenericPEStoreHash<PrefixEquivalenceSOP, nodeEqHash, DominanceInfos> prefix_equivalence_store;
GenericPEStoreHash<PrefixEquivalenceSOP, nodeEqHash, DominanceInfos2> prefix_equivalence_store;

// GenericPEStoreHash<PESOPfb, nodeEqHashFB, DominanceInfos> prefix_equivalence_store;

/**
 * print statistics at the end of the search
 */
void handle_end_search() {
    search_manager.printStats();
    prefix_equivalence_store.printStats();
    exit(1);
}


int main(int argc, char* argv[]) {
    if ( argc < 3 ) {
        std::cout << "\n[ERROR] USAGE: " << argv[0] << " INSTANCE_NAME TIME_LIMIT [OUTPUT_FILENAME]" << std::endl;
        return 1;
    }

    // parse user input
    InstanceSOP inst(argv[1]);
    std::cout << "###\t" << argv[1] << std::endl;
    int time_limit = std::stoi(argv[2]);
    std::string output_filename = "out.sol";
    if ( argc == 4 ) {
        output_filename = argv[3];
    }

    /** create event handlers */
    signal(SIGINT, [](int signal) {
        fprintf(stderr, "Error user kill: signal %d:\n", signal);
        handle_end_search();
        exit(1);
    });


    // define root of the tree
    NodePtr root_ptr = NodePtr(new NodeForw(inst, output_filename));
    // NodePtr root_ptr = NodePtr(new NodeBack(inst, output_filename));
    // NodePtr root_ptr = NodePtr(new NodeFB(inst, output_filename));

    root_ptr = NodePtr(new StatsCombinator<PrefixEquivalenceSOP>(root_ptr, search_manager, search_manager.getSearchStats(), false));

    // root_ptr = NodePtr(new DominanceCombinator<PrefixEquivalenceSOP>(root_ptr, search_manager, prefix_equivalence_store));
    root_ptr = NodePtr(new DominanceCombinator2<PrefixEquivalenceSOP>(root_ptr, search_manager, prefix_equivalence_store));

    // root_ptr = NodePtr(new StatsCombinator<PESOPfb>(root_ptr, search_manager, search_manager.getSearchStats(), false));
    // root_ptr = NodePtr(new DominanceCombinator<PESOPfb>(root_ptr, search_manager, prefix_equivalence_store));

    // start search manager
    search_manager.start();

    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};

    // auto ts = IterativeBeamSearch(ts_params, 1, 2, true);
    auto ts = AnytimePackSearch(ts_params, 2);
    ts.run(time_limit);

    // std::cout << search_manager.getSearchStats().jsonStats() << std::endl;
    // search_manager.writeAnytimeCurve("perfprofile/prob_bs3.json", "probing BS(3)");
    // search_manager.writeNodeOpeningEvents("perfprofile/BS_PE.csv");
    handle_end_search();
    return 0;
}
