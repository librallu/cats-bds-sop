#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <utility>
#include <algorithm>

#include "../../cats-framework/include/io.hpp"
#include "../../cats-framework/include/util/SubsetInt.hpp"

using namespace cats;

namespace sop_bds {

typedef uint16_t NodeId;
typedef int32_t Weight;
typedef std::pair<NodeId, NodeId> Precedence;

double infty = 9999999;

/**
 * \brief represents a SOP instance
 * the constructor parses a sop file at the TSPLIB format
 */
class InstanceSOP {
 public:
    explicit InstanceSOP(std::string filename) {
        std::ifstream f;
        f.open(filename);
        if ( !f.is_open() ) {
            throw FileNotFoundException();
        }

        f >> n_;
        for ( int i = 0 ; i < n_ ; i++ ) {
            std::vector<Weight> tmp;
            for ( int j = 0 ; j < n_ ; j++ ) {
                Weight tmp_v;
                f >> tmp_v;
                if ( tmp_v < 0 ) {
                    precedences_.push_back(Precedence(j, i));
                }
                tmp.push_back(tmp_v);
            }
            matrix_.push_back(tmp);
        }

        // update diagonal weights
        for ( uint32_t i = 0 ; i < matrix_[0].size() ; i++ ) {
            matrix_[i][i] = infty;
        }

        // compute vertices that precede some other vertices
        nb_precedence_vertices_ = 0;

        precedence_vertices_.push_back(true);
        for ( int i = 1 ; i < n_-1 ; i++ ) {
            precedence_vertices_.push_back(false);
            for ( int j = 1 ; j < n_-1 ; j++ ) {
                if ( matrix_[j][i] < 0 ) {
                    nb_precedence_vertices_++;
                    precedence_vertices_[i] = true;
                    break;
                }
            }
        }
        precedence_vertices_.push_back(false);

        // compute precedences_for
        for ( int i = 0 ; i < n_ ; i++ ) {
            precedences_for_.push_back(std::vector<NodeId>());
            successors_of_.push_back(std::vector<NodeId>());
            preds_sets_.push_back(SubsetInt());
        }
        for ( int i = 1 ; i < n_ ; i++ ) {
            for ( int j = 1 ; j < n_ ; j++ ) {
                if ( matrix_[j][i] < 0 ) {
                    precedences_for_[j].push_back(i);
                    preds_sets_[j].add(i);
                    successors_of_[i].push_back(j);
                }
            }
        }
        for ( int i = 1 ; i < n_ ; i++ ) {
            successors_of_[0].push_back(i);
        }

        for ( int i = 0 ; i < n_ ; i++ ) {
            min_ingoing_.push_back(infty);
            min_outgoing_.push_back(infty);
            for ( int j = 0 ; j < n_ ; j++ ) {
                if ( matrix_[i][j] >= 0 && i != j ) {
                    min_outgoing_[i] = std::min(min_outgoing_[i], matrix_[i][j]);
                }
                if ( matrix_[j][i] >= 0 && i != j ) {
                    min_ingoing_[i] = std::min(min_ingoing_[i], matrix_[j][i]);
                }
            }
        }

        for ( int i = 0 ; i < n_ ; i++ ) {
            possible_preds_.push_back(std::vector<NodeId>());
        }

        // compute rank decision
        for ( int i = 0 ; i < n_ ; i++ ) {
            rank_decision_.push_back(std::vector<NodeId>());
            for ( int j = 0 ; j < n_ ; j++ ) {
                rank_decision_[i].push_back(n_);
            }

            // create sorted arrays by rank
            std::vector<NodeId> ranks;
            for ( int j = 0 ; j < n_ ; j++ ) {
                ranks.push_back(j);
            }
            std::sort(std::begin(ranks), std::end(ranks), [this, i](NodeId a, NodeId b) {
                return this->get_weight(i, a) < this->get_weight(i, b);
            });
            neighbours_by_rank_.push_back(ranks);

            int r = 0;
            for ( int k : ranks ) {
                if ( k != i && get_weight(i, k) >= 0 ) {
                    rank_decision_[i][k] = r++;
                    possible_preds_[k].push_back(i);
                }
            }
        }
    }


    std::string get_matrix_string() const {
        std::string res = "";
        for ( int i = 0 ; i < n_ ; i++ ) {
            for ( int j = 0 ; j < n_ ; j++ ) {
                res += std::to_string(matrix_[i][j]);
                res += "\t";
            }
            res += "\n";
        }
        res += "nb precedence vertices: " + std::to_string(nb_precedence_vertices_) + "\n";
        return res;
    }

    std::string to_string() const {
        std::string res = "";
        res += get_matrix_string();
        for ( auto& e : precedences_ ) {
            res += std::to_string(e.first) + "\t->\t" + std::to_string(e.second) + "\n";
        }
        res += "precedence for: \n";
        for ( int i = 0 ; i < n_ ; i++ ) {
            for ( int j : precedences_for_[i] ) {
                res += std::to_string(i) + "\t->\t" + std::to_string(j) + "\n";
            }
        }
        res += "precedence vertices: ";
        for ( int i = 0 ; i < n_ ; i++ ) {
            if ( precedence_vertices_[i] ) {
                res += std::to_string(i) + " ";
            }
        }
        res += "\nmin ingoing: ";
        for ( int i = 0 ; i < n_ ; i++ ) {
            res += std::to_string(min_ingoing_[i]) + " ";
        }
        res += "\nmin outgoing: ";
        for ( int i = 0 ; i < n_ ; i++ ) {
            res += std::to_string(min_outgoing_[i]) + " ";
        }
        res += "\nrank decisions at 0: ";
        for ( int i = 0 ; i < n_ ; i++ ) {
            res += std::to_string(get_rank_decision(0, i)) + " ";
        }
        res += "\nrank decisions at 1: ";
        for ( int i = 0 ; i < n_ ; i++ ) {
            res += std::to_string(get_rank_decision(1, i)) + " ";
        }
        res += "\n";
        res += "lb ingoing: "+std::to_string(root_lb_ingoing())+"\tlb outgoing: "+std::to_string(root_lb_outgoing())+"\n";
        return res;
    }

    inline Weight get_weight(NodeId i, NodeId j) const { return matrix_[i][j]; }

    inline Weight get_weight_undirected(NodeId i, NodeId j) const {
        if ( matrix_[i][j] < 0 ) return matrix_[j][i];
        if ( matrix_[j][i] < 0 ) return matrix_[i][j];
        return std::min(matrix_[i][j], matrix_[j][i]);
    }

    inline std::vector<Weight> get_neighbours(NodeId i) const { return matrix_[i]; }

    inline Weight root_lb_ingoing() const {
        Weight res = 0;
        for ( int i = 1 ; i < n_ ; i++ ) {
            Weight e = min_ingoing_[i];
            res += e;
        }
        return res;
    }

    inline Weight root_lb_outgoing() const {
        Weight res = 0;
        for ( int i = 0 ; i < n_-1 ; i++ ) {
            Weight e = min_outgoing_[i];
            res += e;
        }
        return res;
    }

    inline int get_nb_vertices() const { return n_; }

    /**
     * \brief returns vertices that are precedences of i
     */
    inline const std::vector<NodeId>& precedences_of(NodeId i) {
        return precedences_for_[i];
    }

    inline bool is_precedence(NodeId i, NodeId j) const { return matrix_[j][i] < 0; }

    inline Weight get_min_ingoing(NodeId i) const { return min_ingoing_[i]; }

    inline Weight get_min_outgoing(NodeId i) const { return min_outgoing_[i]; }

    inline std::vector<Precedence> getPrecedences() const { return precedences_; }

    inline int get_rank_decision(int i, int j) const { return rank_decision_[i][j]; }

    inline const std::vector<NodeId>& get_neighbours_by_rank(int i) { return neighbours_by_rank_[i]; }

    inline const NodeId getDestinationVertex() { return n_-1; }

    inline const NodeId getStartVertex() { return 0; }

    inline const std::vector<NodeId>& get_possible_preds(NodeId i) { return possible_preds_[i]; }

    inline const SubsetInt& get_pred_set(NodeId i) { return preds_sets_[i]; }

    /**
     * get direct successors of i (with precedence constraints)
     */
    inline const std::vector<NodeId>& successors_of(NodeId i) { return successors_of_[i]; }

 private:
    int n_;
    std::vector<std::vector<Weight>> matrix_;
    std::vector<Precedence> precedences_;
    int nb_precedence_vertices_;
    std::vector<bool> precedence_vertices_;
    std::vector<Weight> min_ingoing_;
    std::vector<Weight> min_outgoing_;
    std::vector<std::vector<NodeId>> precedences_for_;
    std::vector<SubsetInt> preds_sets_;
    std::vector<std::vector<NodeId>> successors_of_;
    std::vector<std::vector<NodeId>> rank_decision_;
    std::vector<std::vector<NodeId>> neighbours_by_rank_;
    std::vector<std::vector<NodeId>> possible_preds_;
};

}  // namespace sop_bds
