#pragma once
#include <string>
#include <iostream>
#include <cassert>
#include <cmath>
#include <memory>
#include <random>

#include "../../cats-framework/include/Node.hpp"
#include "../../cats-framework/include/numeric.hpp"
#include "../../cats-framework/include/util/SubsetInt.hpp"
#include "../../cats-framework/include/combinators/PrefixEquivalenceCombinator.hpp"

#include "InstanceSOP.hpp"
#include "StoreSOP.hpp"
#include "checker.hpp"

using namespace cats;

namespace sop_bds {

/**
 * SOP forward+backward prefixEquivalence
 */
struct PESOPfb {
    SubsetInt subset;
    int first;
    int last;

    PESOPfb(const PESOPfb& n) : subset(n.subset), first(n.first), last(n.last) {}
    PESOPfb(const SubsetInt sub, int f, int l) : subset(sub), first(f), last(l) {}

    bool operator==(const PESOPfb& a) const {
        if ( last != a.last || first != a.first ) return false;
        return subset == a.subset;
    }

    friend std::ostream &operator<<(std::ostream& out, const PESOPfb& n) {
        out << "(" << n.first << "," << n.last << ")";
        return out;
    }
};

/**
 * hash function taking a nodeEquivalenceSOP as a parameter
 */
struct nodeEqHashFB {
    size_t operator()(const PESOPfb& n) const noexcept {
        size_t seed = n.subset.hash();
        boost::hash_combine(seed, static_cast<size_t>(n.last));
        boost::hash_combine(seed, static_cast<size_t>(n.first));
        return seed;
    }
};

class NodeFB : public PrefixEquivalenceNode<PESOPfb> {
 private:
    InstanceSOP& inst_;
    std::vector<NodeId> prefix_;
    std::vector<NodeId> suffix_;
    Weight cost_;
    SubsetInt added_subset_;
    SubsetInt prefix_set_;
    SubsetInt suffix_set_;
    std::string& output_filename_;

 public:
    explicit NodeFB(InstanceSOP& inst, std::string& output_filename): Node(),
        inst_(inst), cost_(0), added_subset_(), prefix_set_(), suffix_set_(), output_filename_(output_filename) {
            prefix_.push_back(inst_.getStartVertex());
            suffix_.push_back(inst_.getDestinationVertex());
        }

    explicit NodeFB(const NodeFB& s): Node(s),
        inst_(s.inst_), prefix_(s.prefix_), suffix_(s.suffix_), cost_(s.cost_),
        added_subset_(s.added_subset_), prefix_set_(s.prefix_set_), suffix_set_(s.suffix_set_), output_filename_(s.output_filename_) {}

    NodePtr copy() const override { return NodePtr(new NodeFB(*this)); }

    double evalPrefix() const override {
        return cost_;
    }

    NodeId lastSuffix() const {
        return suffix_[suffix_.size()-1];
    }

    NodeId lastPrefix() const {
        return prefix_[prefix_.size()-1];
    }

    /**
     * Applies a forward or backward operation depending on the size of possibles 
     * (i.e. if a backward step has less children than the forward step, use the backward step)
     */
    std::vector<NodePtr> getChildren() override {
        NodeId last_city_prefix = lastPrefix();
        NodeId last_city_suffix = lastSuffix();
        // FORWARD COMPUTATION
        std::vector<NodePtr> forward;
        for ( NodeId neigh : inst_.get_neighbours_by_rank(last_city_prefix) ) {
            // for each vertex, check if we can add it
            bool to_add = neigh != last_city_prefix && neigh != last_city_suffix && !added_subset_.contains(neigh);  // check that it is not already added
            if ( to_add ) {
                for ( NodeId pred : inst_.precedences_of(neigh) ) {  // if not, check all precedences of neigh
                    if ( pred != last_city_prefix && !prefix_set_.contains(pred) ) {
                        to_add = false;
                        break;
                    }
                }
            }
            if ( to_add ) {  // generate children and add it to the result
                NodeFB* child = new NodeFB(*this);
                child->add_city_prefix(neigh);
                forward.push_back(NodePtr(child));
            }
        }
        // BACKWARD COMPUTATION
        std::vector<NodePtr> backward;
        
        for ( NodeId neigh : inst_.get_possible_preds(last_city_suffix) ) {  // for all neighbours
            bool to_add = neigh != last_city_prefix && neigh != last_city_suffix && !added_subset_.contains(neigh);  // check that it is not already added
            if ( to_add ) {
                for ( NodeId succ : inst_.successors_of(neigh) ) {  // check if some successors are not added
                    if ( succ != last_city_suffix && !suffix_set_.contains(succ) ) {
                        to_add = false;
                        break;
                    }
                }
            }
            if ( to_add ) {  // generate children and add it to the result
                NodeFB* child = new NodeFB(*this);
                child->add_city_suffix(neigh);
                backward.push_back(NodePtr(child));
            }
        }
        return forward.size() < backward.size() ? forward : backward;
    }

    inline bool isGoal() const override {
        return inst_.get_nb_vertices() == static_cast<int>(prefix_.size()+suffix_.size());
    }

    /**
     * \brief called when a new best solution is found. Checks that the solution is feasible. If not, explain why
     */
    void handleNewBest() override {
        std::vector<NodeId> res = getsol();
        double check_val = checker(inst_, res);
        assert(check_val == this->evaluate());
        // write solution file
        std::ofstream f;
        f.open(output_filename_);
        for ( NodeId p : res ) {
            f << p << " ";
        }
        f << "\n";
        f.close();
    }

    std::vector<NodeId> getsol() {
        std::vector<NodeId> res = std::vector<NodeId>();
        for ( auto e : prefix_ ) {
            res.push_back(e);
        }
        for ( int i = suffix_.size()-1 ; i >= 0 ; i-- ) {
            res.push_back(suffix_[i]);
        }
        return res;
    }

    PESOPfb getPrefixEquivalence() const override {
        return PESOPfb(added_subset_, lastPrefix(), lastSuffix());
    }

    std::string getName() override {
        return "forw+back";
    }

 private:
    /**
     * \brief adds a given city to the current node prefix
     */
    void add_city_prefix(NodeId j) {
        NodeId i = lastPrefix();
        added_subset_.add(i);
        prefix_set_.add(i);
        prefix_.push_back(j);
        // update bounds
        cost_ += inst_.get_weight(i, j);
        checkFinish();
    }
    /**
     * \brief adds a given city to the current node suffix
     */
    void add_city_suffix(NodeId j) {
        NodeId i = lastSuffix();
        added_subset_.add(i);
        suffix_set_.add(i);
        suffix_.push_back(j);
        // update bounds
        cost_ += inst_.get_weight(j, i);
        checkFinish();
    }

    /**
     * if |prefix|+|suffix| = n, add cost from last prefix and last suffix
     */
    void checkFinish() {
        if ( isGoal() ) {
            cost_ += inst_.get_weight(lastPrefix(), lastSuffix());
        }
    }
};

}  // namespace sop_bds
