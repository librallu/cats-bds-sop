#pragma once
#include <string>
#include <iostream>
#include <cassert>
#include <cmath>
#include <memory>

#include "../../cats-framework/include/Node.hpp"

#include "InstanceSOP.hpp"
#include "StoreSOP.hpp"
#include "checker.hpp"

using namespace cats;

namespace sop_bds {

class NodeBack : public PrefixEquivalenceNode<PrefixEquivalenceSOP> {
 private:
    InstanceSOP& inst_;
    std::vector<NodeId> prefix_;
    Weight cost_prefix_;
    SubsetInt added_subset_;
    std::string& output_filename_;

 public:
    explicit NodeBack(InstanceSOP& inst, std::string& output_filename): Node(),
        inst_(inst), cost_prefix_(0), added_subset_(), output_filename_(output_filename) {
            prefix_.push_back(inst_.getDestinationVertex());
            for ( int i = 0 ; i < inst_.get_nb_vertices()-1 ; i++ ) {
                added_subset_.add(i);
            }
        }

    explicit NodeBack(const NodeBack& s): Node(s),
        inst_(s.inst_), prefix_(s.prefix_), cost_prefix_(s.cost_prefix_),
        added_subset_(s.added_subset_), output_filename_(s.output_filename_) {}

    NodePtr copy() const override { return NodePtr(new NodeBack(*this)); }

    double evalPrefix() const override {
        return cost_prefix_;
    }

    double evaluate() const override {
        return evalPrefix();
    }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> res;
        NodeId last_vertex = lastAdded();
        for ( NodeId neigh : inst_.get_possible_preds(last_vertex) ) {  // for all neighbours
            bool to_add = neigh != last_vertex && added_subset_.contains(neigh);  // check that it is not already added
            if ( to_add ) {
                for ( NodeId succ : inst_.successors_of(neigh) ) {  // check if some successors are not added
                    if ( added_subset_.contains(succ) ) {
                        to_add = false;
                        break;
                    }
                }
            }
            if ( to_add ) {  // generate children and add it to the result
                NodeBack* child = new NodeBack(*this);
                child->add_city(neigh);
                res.push_back(NodePtr(child));
            }
        }
        // std::cout << std::endl;
        return res;
    }

    inline bool isGoal() const override { return inst_.get_nb_vertices() == static_cast<int>(prefix_.size()); }

    /**
     * \brief called when a new best solution is found. Checks that the solution is feasible. If not, explain why
     */
    void handleNewBest() override {
        std::vector<NodeId> sol = prefix_;
        std::reverse(sol.begin(), sol.end());
        double check_val = checker(inst_, sol);
        assert(check_val == this->evaluate());
    }

    PrefixEquivalenceSOP getPrefixEquivalence() const override {
        return PrefixEquivalenceSOP(added_subset_, lastAdded());
    }

    std::string getName() override {
        return "back";
    }

    NodeId lastAdded() const {
        return prefix_[prefix_.size()-1];
    }

 private:
    /**
     * \brief adds a given city to the current node
     */
    void add_city(NodeId j) {
        NodeId i = lastAdded();
        added_subset_.remove(j);
        prefix_.push_back(j);
        // update bounds
        Weight w = inst_.get_weight(j, i);
        cost_prefix_ += w;
    }
};

}  // namespace sop_bds
